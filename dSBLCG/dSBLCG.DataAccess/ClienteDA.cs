﻿using dSBLCG.Entidades;
using dsCore.Tipos;
using dsCore2.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace dSBLCG.DataAccess
{
    public class ClienteDA : DataDirector<Cliente>
    {

        public ClienteDA(IConfiguration configuration)
        {
            var cc = configuration.GetConnectionString("DefaultConnection");
            _conexion = new SqlConnection(cc);
        }

        #region Campos
        #endregion

        #region Constructores      

        #endregion

        #region Metodos de Creacion

        public static ClienteDA CreaAdaptadorParaInsercionModificacionOBorrado(Cliente Cliente, IConfiguration configuration)
        {
            var mapper = new ClienteDA(configuration) { EntidadNegocio = Cliente };
            return mapper;
        }

        public static ClienteDA CreaAdaptadorParaLecturaUnica(int idCliente, IConfiguration configuration)
        {
            var mapper = new ClienteDA(configuration);
            mapper.EntidadNegocio = mapper.CrearNuevaEntidadNegocio();
            mapper.EntidadNegocio.IDCliente = idCliente;
            return mapper;
        }

        public static ClienteDA CreaAdaptadorParaLista(IConfiguration configuration)
        {
            var mapper = new ClienteDA(configuration);
            return mapper;
        }


        #endregion

        #region Metodos

        protected override TTipoTransaccion? TransaccionRecibida
        {
            get { return EntidadNegocio.TipoTrans; }
        }

        public List<Cliente> LeerClientes()
        {
            var res = new List<Cliente>();
            try
            {
                _cmd = SQLHelper.PrepareCommand(_conexion, null, CommandType.StoredProcedure, @"Aplicacion.LeerClientes", null, 0);
                _reader = _cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (_reader.Read())
                {
                    res.Add(RellenarCliente());
                }

            }
            finally
            {
                CerrarObjetosDeBaseDatos();
            }
            return res;
        }

        private Cliente RellenarCliente()
        {
            return new Cliente
            {
                IDCliente = AsignaEntero("IDCliente"),
                TS = AsignaArrayByte("TS")
            };
        }

        protected override void InicializarParaRellenarEntidadNegocio()
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("@idCliente", EntidadNegocio.IDCliente) };
            _cmd = SQLHelper.PrepareCommand(_conexion, null, CommandType.StoredProcedure, @"Aplicacion.ClienteLeerPorID", param, 0);
        }

        protected override void RellenarEntidadNegocio()
        {
            EntidadNegocio.IDCliente = AsignaEntero("IDCliente");
            
            EntidadNegocio.TS = AsignaArrayByte("TS");
        }

        protected override void InicializarParaProcesoCompleto()
        {
            string ClienteXml = dsCore.Comun.Ayudas.SerializarACadenaXML(EntidadNegocio);
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("@ClienteXML", ClienteXml) };
            _cmd = SQLHelper.PrepareCommand(_conexion, null, CommandType.StoredProcedure, @"Aplicacion.Cliente_Procesar", param, 0);
        }


        protected override void PostProcesoCompleto()
        {
        }

        protected override void InicializarParaBorrado()
        {
        }

        protected override void InicializarParaModificacion()
        {
        }

        protected override void InicializarParametrosComunes()
        {
        }

        protected override void PostBorrado()
        {
        }

        protected override void PostInsercion()
        {
        }

        protected override void PostModificacion()
        {
        }

        protected override void InicializarParaInsercion()
        {
        }

        protected override void RellenarEntidadCompleta()
        {
            if (_reader.Read())
                RellenarEntidadNegocio();

            _reader.NextResult();



        }

        public new Cliente LeerUno()
        {
            try
            {
                InicializarParaRellenarEntidadNegocio();
                _reader = _cmd.ExecuteReader(CommandBehavior.CloseConnection);
                RellenarEntidadCompleta();
            }
            finally
            {
                CerrarObjetosDeBaseDatos();
            }

            return EntidadNegocio;
        }



        #endregion
    }
}
