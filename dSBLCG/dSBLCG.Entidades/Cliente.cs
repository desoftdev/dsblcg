﻿using dsCore.Tipos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace dSBLCG.Entidades
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        public int IDCliente { get; set; }
        [DataMember]
        public TTipoTransaccion TipoTrans { get; set; }
        [DataMember]
        public byte[] TS { get; set; }
    }
}
