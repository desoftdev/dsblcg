﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace dSBLCG.Entidades
{
    public class LoginDataModel
    {
        [Required(ErrorMessage = "Usuario requerido")]
        public string userName { get; set; }

        [Required(ErrorMessage = "Contraseña requerida")]
        public string pass { get; set; }
    }
}
