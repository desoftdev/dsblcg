﻿using dSBLCG.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Net;
using dSBLCG.Entidades;
using dSBLCG.DataAccess;

namespace dSBLCG.Server.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ClienteController : ControllerBase
    {
        protected IConfiguration _configuration;

        
        public ClienteController(IConfiguration configuration)
        {
            _configuration = configuration;

        }


        [HttpGet]
        public IActionResult LeerClienteXId(int id)
        {
            var ad = ClienteDA.CreaAdaptadorParaLecturaUnica(id, _configuration);
            ObjectResult result;
            try
            {
                var res = ad.LeerUno();
                result = new ObjectResult(res)
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            catch (Exception ex)
            {
                result = new ObjectResult(ex)
                {
                    StatusCode = (int)HttpStatusCode.Conflict
                };
            }
            return result;
        }


        [HttpGet]
        public IActionResult LeerClientes()
        {
            var ad = new ClienteDA(_configuration);
            ObjectResult result;
            try
            {
                var res = ad.LeerClientes();
                result = new ObjectResult(res)
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            catch (Exception ex)
            {
                result = new ObjectResult(ex)
                {
                    StatusCode = (int)HttpStatusCode.Conflict
                };
            }
            return result;
        }

        [HttpPost]
        //[AutorizacionRequerida]
        public IActionResult SalvarCliente([FromBody] Cliente proyecto)
        {
            var ad = ClienteDA.CreaAdaptadorParaInsercionModificacionOBorrado(proyecto, _configuration);
            ObjectResult result;
            try
            {
                ad.SalvarXML();
                result = new ObjectResult(ad.EntidadNegocio)
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            catch (Exception ex)
            {
                result = new ObjectResult(ex)
                {
                    StatusCode = (int)HttpStatusCode.Conflict
                };
            }
            return result;
        }

    }
}
